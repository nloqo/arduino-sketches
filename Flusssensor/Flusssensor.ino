//-----------------------------------------Activate LCD--------------------------------------

#include <LiquidCrystal.h>              //Library for LCD
LiquidCrystal lcd(2, 3, 4, 5, 6, 7);    //Initialize LCD Lib for our application //the numbers are the Digital Pins, which are used for the LCD

//-----------------------------------------Activate W-Lan Communication--------------------------------------

#include <ArduinoJson.h>                //Library for W-Lan Communication
#define SSID "PI_AP" //Service Set Identifier
#define PASSWORD "SQV10500"
#define ip "192.168.42.1" //
#define LED_WLAN 13

#define DEBUG true
#include <SoftwareSerial.h> //Library for the W-Lan module
SoftwareSerial esp8266(11, 12); // RX, TX, Digital Pins required for the W-Lan module

//-----------------------------------------Define variables and pins--------------------------------------

const float refVoltage=5; //reference voltage for analog ins. If a different voltage is applied to A_ref pin, specify here.

const int analogInputs[]={3,4,5,6,7}; //
const int digitalInputs[]={10,13,A1,A2};
const int digitalOutputs[]={8,9,A0};

int shutter532 = LOW; //Shutter write state for green light (532nm)
int shutter355 = LOW; //Shutter write state for ultraviolett light (355nm)

float Power532 = 0.0; //Measured Power at 532 nm
float Power355 = 0.0; //Measured Power at 355 nm

int shutter532Pin = 9; //Shutter write pin for green light (532nm)
const int ledPin532 =  8; //Shutter LED pin for green light (532nm)
int ledState532 = LOW; //Shutter LED state for green light (532nm)
//Missing parts for 532, will not be added

int shutter355Pin = A0; //Shutter write pin for UV light (355nm)
const int ledPin355 =  A1; //Shutter LED pin for UV light (355nm)
int ledState355 = LOW; //Shutter LED state for UV light (355nm)
const int AIMPin355 =  A3; //Shutter AIM pin for UV light (355nm)
int AIMState355 = LOW;//Shutter AIM state for UV light (355nm)
const int OPENPin355 =  A2;//Shutter OPEN pin for UV light (355nm)
int OPENState355 = LOW;//Shutter OPEN state for UV light (355nm)

unsigned long previousMillis = 0;
const long interval = 1500;
const long intervalemergency = 500;
const long intervalrasp = 5000;

float endtime = 0;
float starttime = 0;

//-----------------------------------------Setup runs once--------------------------------------

void setup() {
  //set reference voltage for analog ins
  if (refVoltage==5)  
    analogReference(DEFAULT);
  else
    analogReference(EXTERNAL);
  for (int i=0; i<sizeof(digitalInputs)/sizeof(int); i++){
    pinMode(digitalInputs[i],INPUT_PULLUP);}
  for (int i=0; i<sizeof(digitalOutputs)/sizeof(int); i++){
    pinMode(digitalOutputs[i],OUTPUT);}
  
  Serial.begin(19200); //Start serial connection to computer
  esp8266.begin(19200); //start W-Lan module
  if (!espConfig()) serialDebug();
  else digitalWrite(LED_WLAN, HIGH);
  sendCom("AT+CIPMODE=0");
  
  lcd.begin(16, 2); //Initialize LCD
  lcd.setCursor(0, 0);//Place on the LCD
  lcd.print("Mira+OPO: ");
}

//-----------------------------------------Loop runs continously--------------------------------------

void loop() {
  
    unsigned long currentMillis = millis();
    
//--------------------------------Determine Analog in A6 value (Shutter Control Value 355)---------------------------
 
//  lcd.setCursor(0, 0);//Place on the LCD
//  lcd.print("Mira+OPO: ");
  int resettime = millis() / 1000;
  delay(1);
  int FlussREGEN = analogRead(7);
  int FlussMIRAOPO = analogRead(6);
//  int FlussMIRAOPO = ReadAnalog(7,1000);
//  int FlussREGEN = ReadAnalog(6,1000);
//  int FlussneuMIRAOPO = Flowrate(7,1000,1000);
//  int FlussneuREGEN = Flowrate(6,1000,1000);  
//  lcd.setCursor(10, 0);//Place on the LCD
//  lcd.print(FlussMIRAOPO);
//  lcd.setCursor(0, 1);//Place on the LCD
//  lcd.print("Regen: ");
//  lcd.setCursor(8, 1);//Place on the LCD
//  lcd.print(FlussREGEN);
//  lcd.setCursor(14, 0);
//  lcd.print("   ");
//  lcd.setCursor(14, 0);
//  lcd.print(analogRead(6));
//  lcd.setCursor(14, 1);
//  lcd.print("   ");
//  lcd.setCursor(14, 1);
//  lcd.print(analogRead(7));
  //lcd.print(resettime);
  delay(1);
//  Serial.print("FlussMIRAOPO: ");
//  Serial.println(FlussMIRAOPO);
//  Serial.print("FlussneuMIRAOPO: ");
//  Serial.println(FlussneuMIRAOPO);
//  Serial.print("FlussREGEN: ");


//
  Serial.println(FlussREGEN);
//Serial.println(FlussMIRAOPO);
  
//  Serial.print("FlussneuREGEN: ");
//  Serial.println(FlussneuREGEN);

  
//  float Fluss= flowr(7, 1, 800, 600);


//float Fluss2= flowr(6, 10, 185, 150);
//  Serial.print("Fluss: ");
//  Serial.println(Fluss);
//  Serial.print("Fluss2: ");
//  Serial.println(Fluss2);
  }//End of the loop



//-----------------------------------------own Functions--------------------------------------

//-----------------------------------------averaged analog Read--------------------------------------

float ReadAnalog(int input, int avg)
{
float sensvalue=0;  // read the sensors
  for(int x=1; x<=avg; x++){ // repeat measurement 'numberOfValuesToAverage' times
    sensvalue += analogRead(input); //take value
      }
    sensvalue=sensvalue/avg;
    return sensvalue;
}


//-----------------------------------------Measuring flowrates (Rectangle Voltage)--------------------------------------

float Flowrate(int input, int avg, unsigned long timeout){
unsigned long  starttime=micros();
unsigned long  endtime=micros();
boolean startState=digitalRead(input);
//      Serial.print("Reading FlowSensor:");
//      Serial.println(digitalRead(input));
//      Serial.println("starttime=");
//      Serial.println(starttime);
//      Serial.println("timediff=");
//      Serial.println((micros()-starttime));
//      Serial.println(digitalRead(input)==startState);
while ((micros()-starttime)<=timeout && digitalRead(input)==startState){
      endtime=micros();
//      Serial.print("Reading FlowSensor:");
//      Serial.println(digitalRead(input));
}
unsigned long deltatime=endtime-starttime;
float flow=500000.0/deltatime;
return flow;
}


//-----------------------------------------Measuring flowrates (Rectangle Voltage)--------------------------------------


float flowr(int input, int Averages, int Maxvalue, int Minvalue)
  { float sumtime = 0;
    for (int i=0; i <= Averages; i++){
do
{starttime=millis();
delay(20);
Serial.println("Alive");
Serial.println(analogRead(input));
} while (analogRead(input)>Maxvalue);
do
{endtime = millis();
delay(20);
Serial.println("Alive2");
Serial.println(analogRead(input));
} while (analogRead(input)>Minvalue);
sumtime=sumtime+(endtime-starttime);} 
float flowrate = sumtime/Averages;
return flowrate;
}


//-----------------------------------------Alarm sound (inactive)--------------------------------------

void alarm(int SENSOR, int value, int PIEZO)
{  debug("alarm!");
  while (analogRead(SENSOR) >= 500)
  {
    tone(PIEZO, 400, 500);
    delay(500);
    tone(PIEZO, 800, 500);
    delay(500);
  }
}

//-----------------------------------------Basic communication between arduino and raspberry--------------------------------------

boolean sendRaspPost(String laserName, String laserStatus)
{
  boolean succes = true;
  String  Host = ip;
  String msg = "field1=" + String("message");
  succes &= sendCom("AT+CIPSTART=\"TCP\",\"" + Host + "\",12345", "OK");

  String postRequest = JsonMessage(laserName,laserStatus);

  if(succes)
  {
    if (sendCom("AT+CIPSEND=" + String(postRequest.length()), ">"))
    {
      esp8266.print(postRequest);
      esp8266.find("SEND OK");
      int i=0;
        for (i=1;i<100;i++){ if(esp8266.available())
        Serial.write(esp8266.read());}
      if (!esp8266.find("CLOSED")) succes &= sendCom("AT+CIPCLOSE", "OK");
    }
    else
    {
      succes = false;
    }
  } 
  return succes;
}


String JsonMessage(String laserName, String laserStatus){
//
// Step 1: Reserve memory space
//
StaticJsonBuffer<200> jsonBuffer;

//
// Step 2: Build object tree in memory
//
JsonObject& root = jsonBuffer.createObject();
root["name"] = laserName;
root["status"] = laserStatus;

//JsonArray& data = root.createNestedArray("data");
//data.add(48.756080, 6);  // 6 is the number of decimals to print
//data.add(2.302038, 6);   // if not specified, 2 digits are printed

//
// Step 3: Generate the JSON string
//
char buff[256];
root.printTo(buff, sizeof(buff));
return buff;
}


//-----------------------------------------Config ESP8266------------------------------------

boolean espConfig()
{
  boolean succes = true;

  esp8266.setTimeout(5000);
  succes &= sendCom("AT+RST", "ready");
  esp8266.setTimeout(1000);

  if (configStation(SSID, PASSWORD)) {
    succes &= true;
    debug("WLAN Connected");
    debug("My IP is:");
    debug(sendCom("AT+CIFSR"));
  }
  else
  {
    succes &= false;
  }
  //shorter Timeout for faster wrong UPD-Comands handling
  succes &= sendCom("AT+CIPMUX=0", "OK");
  succes &= sendCom("AT+CIPMODE=0", "OK");

  return succes;
}

boolean configTCPServer()
{
  boolean succes = true;

  succes &= (sendCom("AT+CIPMUX=1", "OK"));
  succes &= (sendCom("AT+CIPSERVER=1,80", "OK"));

  return succes;

}

boolean configTCPClient()
{
  boolean succes = true;

  succes &= (sendCom("AT+CIPMUX=0", "OK"));
  //succes &= (sendCom("AT+CIPSERVER=1,80", "OK"));

  return succes;

}


boolean configStation(String vSSID, String vPASSWORT)
{
  boolean succes = true;
  succes &= (sendCom("AT+CWMODE=1", "OK"));
  esp8266.setTimeout(20000);
  succes &= (sendCom("AT+CWJAP=\"" + String(vSSID) + "\",\"" + String(vPASSWORT) + "\"", "OK"));
  esp8266.setTimeout(1000);
  return succes;
}

boolean configAP()
{
  boolean succes = true;

  succes &= (sendCom("AT+CWMODE=2", "OK"));
  succes &= (sendCom("AT+CWSAP=\"NanoESP\",\"\",5,0", "OK"));

  return succes;
}

boolean configUDP()
{
  boolean succes = true;

  succes &= (sendCom("AT+CIPMODE=0", "OK"));
  succes &= (sendCom("AT+CIPMUX=0", "OK"));
  succes &= sendCom("AT+CIPSTART=\"UDP\",\"192.168.255.255\",90,91,2", "OK"); //Importand Boradcast...Reconnect IP
  return succes;
}


//-----------------------------------------------Controll ESP-----------------------------------------------------

boolean sendUDP(String Msg)
{
  boolean succes = true;

  succes &= sendCom("AT+CIPSEND=" + String(Msg.length() + 2), ">");    //+",\"192.168.4.2\",90", ">");
  if (succes)
  {
    succes &= sendCom(Msg, "OK");
  }
  return succes;
}


boolean sendCom(String command, char respond[])
{
  esp8266.println(command);
  if (esp8266.findUntil(respond, "ERROR"))
  {
    return true;
  }
  else
  {
    debug("ESP SEND ERROR: " + command);
    return false;
  }
}

String sendCom(String command)
{
  esp8266.println(command);
  return esp8266.readString();
}


//-------------------------------------------------Debug Functions------------------------------------------------------
void serialDebug() {
  while (true)
  {
    if (esp8266.available())
      Serial.write(esp8266.read());
    if (Serial.available())
      esp8266.write(Serial.read());
  }
}

void debug(String Msg)
{
  if (DEBUG)
  {
    Serial.println(Msg);
  }
}
