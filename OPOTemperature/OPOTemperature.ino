
/*
 ***************************
 * Read sensor values and calculate a PID value.
 * Depending on this PID value, a relay is switched.
 ***************************
*/
const String version = "OPOTemperature 1.0"; // Version of this script

//**         CONSTANTS               **//
const int cyclePeriod = 1000; //program cycle period in ms

// I/O-pins
const int sensor0Pin = 0; //Channel to which the sensor 0 is connected
const int sensor1Pin = 1; //Channel to which the sensor 1 is connected
const int sensor2Pin = 2; //Channel to which the sensor 2 is connected
const int sensor3Pin = 3; //Channel to which the sensor 2 is connected
const int sensor4Pin = 4; //Channel to which the sensor 2 is connected
const int sensor5Pin = 5; //Channel to which the sensor 2 is connected

const int controlPin = 12; //Pin by which the cooling unit is controlled (11 or 12)
const int indicatorPin = 3; //Pin sending a control signal, indicating if the cooling is on
const int tooColdPin = 2; //Pin being set high, if the temperature is too low
const int tooHotPin = 4; //Pin being set high, if the temperature is too high
const int powerPin = 13; //Pin for indicating device is on; also used to indicate errors

#include <EEPROM.h>
const int floatsize = sizeof(float);

// sensor parameters
long measurements = 100; //How many times should the sensor be read for a single readout?
const int averaging = 50;  // Number of times for the running average
int precision = 5; // Number of digits to print.


// MISC
const long rolloverThreshold = 4294965000;  //After how many milliseconds should we suspend normal operation to await millis()-rollover? (Should be at least cyclePeriod+digitalPinSetDelay below the maximum value of an unsigned long of 4.294.967.295)
// ********************************************************************************


// ***********************************VARIABLES************************************
// Sensor values.
float sensor0Value = 0; //Define sensor value variable
float sensor1Value = 0; //Define sensor value variable
float sensor2Value = 0; //Define sensor value variable
float sensor3Value = 0; //Define sensor value variable
float sensor4Value = 0; //Define sensor value variable
float sensor5Value = 0; //Define sensor value variable

#include "RunningAverage.h"

RunningAverage average0(averaging); //generate running average variable
RunningAverage average1(averaging); //generate running average variable
RunningAverage average2(averaging); //generate running average variable
RunningAverage average3(averaging); //generate running average variable
RunningAverage average4(averaging); //generate running average variable
RunningAverage average5(averaging); //generate running average variable

// As we only want to do one measurement per second, we need to know, when we last ran.
unsigned long nextCycle = 3000; //set starting values for an initial delay of 5s to prevent racing conditions on start
unsigned long nextOff = 0; // When to switch off the heater the next time

//******************************PID-CONTROL*****************************************
// Werte werden aus EEPROM gelesen
float setpoint; // setpoint for the temperature = 36°C
float PIDKp; //Wert für die PID Regelung (Proportional-Teil)
float PIDKi; //Wert für die PID Regelung (Integral-Teil)
float integral; //Wert für die PID Integralteil


int controller = 2;  // controller mode: 0 = off, 1 = control cooling only, 2 = full control

boolean autoData=false;
//******************************************************************************


void setup()  {
  //Setup the serial connection and the pins
  Serial.begin(9600);

  pinMode(tooColdPin,OUTPUT);
  pinMode(tooHotPin,OUTPUT);
  pinMode(indicatorPin, OUTPUT);
  pinMode(controlPin, OUTPUT);
  pinMode(powerPin, OUTPUT);

  digitalWrite(powerPin, HIGH);
  digitalWrite(indicatorPin, HIGH);

  // Read PID values
  EEPROM.get(0 * floatsize, setpoint);
  if ( isnan(setpoint)){
    setpoint = 400;
  }
  EEPROM.get(1 * floatsize, PIDKp);
  if ( isnan(PIDKp)){
    PIDKp = -1000;
  }
  EEPROM.get(2 * floatsize, PIDKi);
  if ( isnan(PIDKi)){
    PIDKi = -1;
  }
  EEPROM.get(3 * floatsize, integral);
  if ( isnan(integral)  || integral < 0 || integral > 1000 ){
    integral = 100;
  }
}


void loop(){
  // Read the sensor many times and calculate the average
  sensor0Value = 0;
  sensor1Value = 0;
  sensor2Value = 0;
  sensor3Value = 0;
  sensor4Value = 0;
  sensor5Value = 0;
  for(int x=1; x <= measurements; x++){
    sensor0Value += analogRead(sensor0Pin);
    sensor1Value += analogRead(sensor1Pin);
    sensor2Value += analogRead(sensor2Pin);
    sensor3Value += analogRead(sensor3Pin);
    sensor4Value += analogRead(sensor4Pin);
    sensor5Value += analogRead(sensor5Pin);
  }

  sensor0Value /= measurements;
  sensor1Value /= measurements;
  sensor2Value /= measurements;
  sensor3Value /= measurements;
  sensor4Value /= measurements;
  sensor5Value /= measurements;
  average0.addValue(sensor0Value);
  average1.addValue(sensor1Value);
  average2.addValue(sensor2Value);
  average3.addValue(sensor3Value);
  average4.addValue(sensor4Value);
  average5.addValue(sensor5Value);

  //Serial communication
  if(autoData){
  sendSensorDataLast();
  }
  if ( Serial.available() ){
    char input[20];
    int len;
    len = Serial.readBytesUntil('\n', input, 20);
    input[len] = '\0';  // Null terminating the array to interpret it as a string
    switch (input[0]){
      case 'p':  // Ping
        Serial.println("Pong");
        break;
      case 'r':  // Send read average sensor data
        sendSensorDataAverage();
        break;
      case 'l':  // Send sensor data
        sendSensorDataLast();
        break;
      case 'm':  //Set number of measurements
        measurements = atoi(&input[1]);
        break;
      case 'f': //Set the precision
        precision = atoi(&input[1]);
        break;
      case 'a': // Automatically send sensor data
        if ( input[1] <= '0' ){
          autoData= false;
          Serial.println("ACK autoOff");
        }
        else {
          autoData=true;
          Serial.println("ACK autoOn");
        }
        break;
      case 'c':  // Configure the controller
        controller = input[1]-'0';
        Serial.println("ACK-controller");
        break;
      case 'k'://Einstellen der PID Parameter
        if ( input[1] == 'p'){
          PIDKp=atof(&input[2]);
          Serial.println("ACK-Kp");
        }
        else if ( input[1] == 'i'){
          PIDKi=atof(&input[2]);
          Serial.println("ACK-Ki");
        }
        else if ( input[1] == 'x'){
          integral=atof(&input[2]);
          Serial.println("ACK-integral");
        }
        else if ( input[1] == 's'){
          setpoint = atof(&input[2]);
        Serial.println("ACK-setpoint");
        }
        break;
      case 'o': // control the relay itself
        if ( input[1] <= '0' ){
          controlRelay( false );
          Serial.println("ACK openRelay");
        }
        else {
          controlRelay( true );
          Serial.println("ACK closeRelay");
        }
        break;
      case 'd': // debug
        Serial.print(integral);
        Serial.print(", p:");
        Serial.print(PIDKp);
        Serial.print(", i:");
        Serial.print(PIDKi);
        Serial.println();
        break;
      case 'v':  // Send Version
        Serial.println(version);
        break;
      case 'e':  // Store values in EEPROM
        EEPROM.put(0 * floatsize, setpoint);
        EEPROM.put(1 * floatsize, PIDKp);
        EEPROM.put(2 * floatsize, PIDKi);
        EEPROM.put(3 * floatsize, integral);
        Serial.println("ACK EEPROM saved.");
        break;
      case 'x':  // get humidity-sensor raw-data readout (specify sensor)
        {
        int sensor = atoi(&input[1]);
        double* readout_result; 
        readout_result = readout(sensor);
        double temp = readout_result[0];
        double hum = readout_result[1];
        Serial.println(sensor);
        Serial.print("temp: ");
        Serial.print(temp);
        Serial.print(", hum: ");
        Serial.print(hum);
        Serial.println();
        break;}

    }
  }

  // Control temperature, if desired
  if ( millis() >= nextCycle ){
    if (controller & 2){
      calculatePID();
    }
    nextCycle += cyclePeriod;
  }

  if ( millis() >= nextOff && nextOff > 0){
    controlRelay(false);
  }

  //if we are within 2300ms of rollover of millis counter suspend normal operation  
  if (millis() > rolloverThreshold){
    nextCycle = cyclePeriod;  //set everything for continuing normal operation after rollover
    while (millis() > rolloverThreshold){};    //wait for rollover to happen
  };  
}


void sendSensorDataLast(){
    // Send the last sensor values.
    Serial.print(sensor0Value, precision);
    Serial.print("\t");
    Serial.print(sensor1Value, precision);
    Serial.print("\t");
    Serial.print(sensor2Value, precision);
    Serial.print("\t");
    Serial.print(sensor3Value, precision);
    Serial.print("\t");
    Serial.print(sensor4Value, precision);
    Serial.print("\t");
    Serial.print(sensor5Value, precision);
    Serial.print("\t");
    Serial.print(setpoint, precision);
    Serial.println();
}


void sendSensorDataAverage(){
    // Send the running average sensor values.
    Serial.print(average0.getAverage(), precision);
    Serial.print("\t");
    Serial.print(average1.getAverage(), precision);
    Serial.print("\t");
    Serial.print(average2.getAverage(), precision);
    Serial.print("\t");
    Serial.print(average3.getAverage(), precision);
    Serial.print("\t");
    Serial.print(average4.getAverage(), precision);
    Serial.print("\t");
    Serial.print(average5.getAverage(), precision);
    Serial.println();
}


void calculatePID(){
  // Calculate the PID signal
  float error = setpoint - average0.getAverage();
  integral += PIDKi * error;
  if (integral > cyclePeriod){
    integral = cyclePeriod;
  }
  else if (integral < 0){
    integral = 0;
  }
  float pid = PIDKp * error + integral;
  if (pid < 0){
    nextOff = 0;
  }
  else {
    nextOff = millis() + pid;
    controlRelay( true );
  }
}

void controlRelay(const boolean close){
  //Open or close the relay
  if ( close ){
    digitalWrite(controlPin, HIGH);
    digitalWrite(indicatorPin, HIGH);
  }
  else {
    digitalWrite(controlPin, LOW);
    digitalWrite(indicatorPin, LOW);
  }
}

double * readout(int sensor) {
  // reads out humidity sensor
  //ArrayData a;
  static double a[2];
  unsigned char result[4];
  int i = 0;
  unsigned int traw;
  unsigned int hraw;
  double temp;
  double hum;

  Wire.begin();
  
  switch (sensor) {
    case 0:
      Wire.beginTransmission(0x25);
      break;
    case 1:
      Wire.beginTransmission(0x26);
      break;
    case 2:
      Wire.beginTransmission(0x27);
      break;
    case 3:
      Wire.beginTransmission(0x28);
      break;
  };
  
  Wire.endTransmission();
  delay(del);
  //Wire.requestFrom(address, 4, true);
  switch (sensor) {
    case 0:
      Wire.requestFrom(0x25, 4, true);
      break;
    case 1:
      Wire.requestFrom(0x26, 4, true);
      break;
    case 2:
      Wire.requestFrom(0x27, 4, true);
      break;
    case 3:
      Wire.requestFrom(0x28, 4, true);
      break;
  };
  i = 0;
  while (Wire.available())
  {
    char c = Wire.read();
    result[i] = c;
    i++;
  }
  traw = result[2] * 256 + result[3];
  hraw = result[0] * 256 + result[1];

  //Daten laut Datenblatt maskieren
  traw &= 0xfffc;
  hraw &= 0x3fff;
  traw >>= 2;

  //Rohdaten Umrechnen
  temp = (double)traw / TFACTOR;
  temp = temp - TDELTA;
  hum = (double)hraw / HFACTOR;


  a[0] = temp;
  a[1] = hum;
  return a;
};
