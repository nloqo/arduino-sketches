
int triggerPin = 2;
int signalPin = 0;
bool triggerOn = false;
int triggerMeasure = 0;
const int length = 20000;
int dataArray[length];

void setup() {
  Serial.begin(921600);
  pinMode(2, INPUT);
  analogReadResolution(10);
}

void loop() {
  triggerMeasure = digitalRead(triggerPin);
  if (triggerOn == false && triggerMeasure == 1){
    triggerOn = true;
    for (int i = 0; i < length; i++){
      dataArray[i] = analogRead(signalPin);
    }
    if ( Serial.available()){
      char input[20];
      int len;
      len = Serial.readBytesUntil('\n', input, 20);
      input[len] = '\0';  // Null terminating the array to interpret it as a string
      switch (input[0]){
        case 'r':
          for (int i = 0; i < length; i++){
            Serial.print(dataArray[i]);
            Serial.print("\t");
          }
          Serial.println();
          break;
      }
    }
  }
  else if (triggerOn == true && triggerMeasure == 0){
    triggerOn = false;
  }
}
