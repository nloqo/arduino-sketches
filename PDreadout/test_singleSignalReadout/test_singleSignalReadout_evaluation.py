# **
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 29 10:27:53 2024

@author: ps-admin
"""

from devices import arduino
import matplotlib.pyplot as plt
import numpy as np

device = arduino.Arduino("COM73", timeout=5000, baud_rate=921600)

data = device.ask("r")
data = data.split("\t")

dat = []
t = []
for i in range(len(data)):
    try:
        dat.append(int(data[i]))
        t.append(i / 114.2)
    except ValueError:
        print(i)

plt.plot(t, np.array(dat) * 3.3 / 4095)
plt.ylabel("Spannung in V")
plt.xlabel("Zeit in ms")

trigger_point = []
for i in range(len(data) - 2):
    if dat[i + 1] > 0 and dat[i] == 0:
        trigger_point.append(i + 1)

rate = []
for i in range(len(trigger_point) - 1):
    rate.append((trigger_point[i + 1] - trigger_point[i]) / 50e-3)
print(f"Abtastrate: ({round(np.mean(rate) / 1e6, 3)} +/- {round(np.std(rate) / 1e6, 3)}) MHz")
print(f"Periode T: {round(1 / np.mean(rate) * 1e9, 0)} ns")
device.close()
del device

plt.show()
