# -*- coding: utf-8 -*-
"""
Created on Sun Apr 28 15:38:17 2024

@author: ps-admin
"""

from devices import arduino
import matplotlib.pyplot as plt


device = arduino.Arduino("COM14", timeout=5000)

data = device.ask("r")
data = data.split("\t")
dat = []
positiveTrigger = 0
for i in range(len(data) - 1):
    try:
        if int(data[i]) == 1:
            positiveTrigger += 1
        dat.append(int(data[i]))
    except ValueError:
        print(i)

rate = len(dat) / int(data[-1]) * 1e6
print(f"Time elapsed: {int(data[-1]) / 1000} ms")
print(f"Rate: {rate / 1e6} MHz")
print(f"Trigger time: {positiveTrigger / rate * 1e6} µs")
plt.plot(dat)

device.close()
