


const String version = "1.0.0";

const short PD0pin = 0;         //Analouge input channel PD0
const short PD1pin = 1;         //Analouge input channel PD1
const short PD2pin = 2;
const short PD3pin = 3;
const short PD4pin = 4;
const short PD5pin = 5;
const short PD6pin = 6;
const short PD7pin = 7;
const short PD8pin = 8;
const short PD9pin = 9;
const short PD10pin = 10;
const short PD11pin = 11;
const short triggerpin = 5;

short activeChannels = 0;

short resolution = 16;
const int dataLength = 20000;
unsigned long sample_rate = 400000000;
unsigned int readLength = 0;
int lastTriggerValue = 0;

float lastData = 0;
float averageData = 0;

bool sendingMode = true;                              // if true: send data whenever new data point is available; otherwise send sum / integral / array if requested
bool sendRaw = false;                                 // if true: send data arrays; otherwise just send the sum or integral
char evaluationMode[] = "int";                        // evaluation mode: int for integral, sum for sum and avg for average

unsigned int data0[dataLength];
unsigned int data1[dataLength];
unsigned int data2[dataLength];
unsigned int data3[dataLength];
unsigned int data4[dataLength];
unsigned int data5[dataLength];
unsigned int data6[dataLength];
unsigned int data7[dataLength];
unsigned int data8[dataLength];
unsigned int data9[dataLength];
unsigned int data10[dataLength];

// Assign the channels to the different ADCs

#include <Arduino_AdvancedAnalog.h>

if ( activeChannels == 1 ){
    AdvancedADC adc1(A7);
  }
else if ( activeChannels == 2 ){
  AdvancedADC adc1(A7);
  AdvancedADC adc2(A0);
}
else if ( activeChannels == 3 ){
AdvancedADC adc1(A7);
AdvancedADC adc2(A0);
AdvancedADC adc3(A5);
}
else if ( activeChannels == 4 ){
  AdvancedADC adc1(A7, A11);
  AdvancedADC adc2(A0);
  AdvancedADC adc3(A5);
}
else if ( activeChannels == 5 ){
  AdvancedADC adc1(A7, A11);
  AdvancedADC adc2(A0, A1);
  AdvancedADC adc3(A5);
}
else if ( activeChannels == 6 ){
  AdvancedADC adc1(A7, A11);
  AdvancedADC adc2(A0, A1);
  AdvancedADC adc3(A5, A6);
}
else if ( activeChannels == 7 ){
  AdvancedADC adc1(A7, A11, A10);
  AdvancedADC adc2(A0, A1);
  AdvancedADC adc3(A5, A6);
}
else if ( activeChannels == 8 ){
  AdvancedADC adc1(A7, A11, A10);
  AdvancedADC adc2(A0, A1, A2);
  AdvancedADC adc3(A5, A6);
}
else if ( activeChannels == 9 ){
  AdvancedADC adc1(A7, A11, A10);
  AdvancedADC adc2(A0, A1, A2);
  AdvancedADC adc3(A5, A6, A8);
}
else if ( activeChannels == 10 ){
  AdvancedADC adc1(A7, A11, A10, A4);
  AdvancedADC adc2(A0, A1, A2);
  AdvancedADC adc3(A5, A6, A8);
}
else if ( activeChannels == 11 ){
  AdvancedADC adc1(A7, A11, A10, A4);
  AdvancedADC adc2(A0, A1, A2, A3); 
  AdvancedADC adc3(A5, A6, A8);
}
else if ( activeChannels == 12 ){
  AdvancedADC adc1(A7, A11, A10, A4);
  AdvancedADC adc2(A0, A1, A2, A3);
  AdvancedADC adc3(A5, A6, A8, A9);
}

#define CHANNELS_1 A7, A11, A10, A4


AdvancedADC adc1(CHANNELS_1);
AdvancedADC adc2(A0, A1, A2, A3);
AdvancedADC adc3(A5, A6, A8, A9);


void readBuffer(){
  if ( activeChannels == 1 ){
    SampleBuffer buf1 = adc1.read();
    for (int i = 0; i < dataLength; i++){
        data0[i] = buf1[i];
    buf1.release();
  }
  else if ( activeChannels == 2){
    SampleBuffer buf1 = adc1.read();
    SampleBuffer buf2 = adc2.read();
    for (int i = 0; i < dataLength; i++){
        data0[i] = buf1[i];
        data1[i] = buf2[i];
    buf1.release();
    buf2.release();
  }
  else if ( activeChannels == 3){
    SampleBuffer buf1 = adc1.read();
    SampleBuffer buf2 = adc2.read();
    SampleBuffer buf3 = adc3.read();
    for (int i = 0; i < dataLength; i++){
        data0[i] = buf1[i];
        data1[i] = buf2[i];
        data2[i] = buf3[i]
    buf1.release();
    buf2.release();
    buf3.release();
  }
  else if ( activeChannels == 4){
    SampleBuffer buf1 = adc1.read();
    SampleBuffer buf2 = adc2.read();
    SampleBuffer buf3 = adc3.read();
    for (int i = 0; i < dataLength; i++){
        data0[i] = buf1[2 * i];
        data1[i] = buf2[i];
        data2[i] = buf3[i];
        data3[i] = buf1[2 * i + 1];
    }
    buf1.release();
    buf2.release();
    buf3.release();
  }
  else if ( activeChannels == 5){
    SampleBuffer buf1 = adc1.read();
    SampleBuffer buf2 = adc2.read();
    SampleBuffer buf3 = adc3.read();
    for (int i = 0; i < dataLength; i++){
        data0[i] = buf1[2 * i];
        data1[i] = buf2[2 * i]
        data2[i] = buf3[i];
        data3[i] = buf1[2 * i + 1];
        data4[i] = buf2[2 * i + 1];
    }
    buf1.release();
    buf2.release();
    buf3.release();
  }
  else if ( activeChannels == 6){
    SampleBuffer buf1 = adc1.read();
    SampleBuffer buf2 = adc2.read();
    SampleBuffer buf3 = adc3.read();
    for (int i = 0; i < dataLength; i++){
        data0[i] = buf1[2 * i];
        data1[i] = buf2[2 * i];
        data2[i] = buf3[2 * i];
        data3[i] = buf1[2 * i + 1];
        data4[i] = buf2[2 * i + 1];
        data5[i] = buf3[2 * i + 1];
    }
    buf1.release();
    buf2.release();
    buf3.release();
  }
  else if ( activeChannels == 7 ){
    SampleBuffer buf1 = adc1.read();
    SampleBuffer buf2 = adc2.read();
    SampleBuffer buf3 = adc3.read();
    for (int i = 0; i < dataLength; i++){
        data0[i] = buf1[3 * i];
        data1[i] = buf2[2 * i];
        data2[i] = buf3[2 * i];
        data3[i] = buf1[3 * i + 1];
        data4[i] = buf2[2 * i + 1];
        data5[i] = buf3[2 * i + 1];
        data6[i] = buf1[3 * i + 2];
    }
    buf1.release();
    buf2.release();
    buf3.release();
  }
  else if ( activeChannels == 8 ){
    SampleBuffer buf1 = adc1.read();
    SampleBuffer buf2 = adc2.read();
    SampleBuffer buf3 = adc3.read();
    for (int i = 0; i < dataLength; i++){
        data0[i] = buf1[3 * i];
        data1[i] = buf2[3 * i];
        data2[i] = buf3[2 * i];
        data3[i] = buf1[3 * i + 1];
        data4[i] = buf2[3 * i + 1];
        data5[i] = buf3[2 * i + 1];
        data6[i] = buf1[3 * i + 2];
        data7[i] = buf2[3 * i + 2];
    }
    buf1.release();
    buf2.release();
    buf3.release();
  }
  else if ( activeChannels == 9 ){
    SampleBuffer buf1 = adc1.read();
    SampleBuffer buf2 = adc2.read();
    SampleBuffer buf3 = adc3.read();
    for (int i = 0; i < dataLength; i++){
        data0[i] = buf1[3 * i];
        data1[i] = buf2[3 * i];
        data2[i] = buf3[3 * i];
        data3[i] = buf1[3 * i + 1];
        data4[i] = buf2[3 * i + 1];
        data5[i] = buf3[3 * i + 1];
        data6[i] = buf1[3 * i + 2];
        data7[i] = buf2[3 * i + 2];
        data8[i] = buf3[3 * i + 2];
    }
    buf1.release();
    buf2.release();
    buf3.release();
  }
  else if ( activeChannels == 10 ){
    SampleBuffer buf1 = adc1.read();
    SampleBuffer buf2 = adc2.read();
    SampleBuffer buf3 = adc3.read();
    for (int i = 0; i < dataLength; i++){
        data0[i] = buf1[4 * i];
        data1[i] = buf2[3 * i];
        data2[i] = buf3[3 * i];
        data3[i] = buf1[4 * i + 1];
        data4[i] = buf2[3 * i + 1];
        data5[i] = buf3[3 * i + 1];
        data6[i] = buf1[4 * i + 2];
        data7[i] = buf2[3 * i + 2];
        data8[i] = buf3[3 * i + 2];
        data9[i] = buf1[4 * i + 3];
    }
    buf1.release();
    buf2.release();
    buf3.release();
  }
  else if ( activeChannels == 11 ){
    SampleBuffer buf1 = adc1.read();
    SampleBuffer buf2 = adc2.read();
    SampleBuffer buf3 = adc3.read();
    for (int i = 0; i < dataLength; i++){
        data0[i] = buf1[4 * i];
        data1[i] = buf2[4 * i];
        data2[i] = buf3[3 * i];
        data3[i] = buf1[4 * i + 1];
        data4[i] = buf2[4 * i + 1];
        data5[i] = buf3[3 * i + 1];
        data6[i] = buf1[4 * i + 2]
        data7[i] = buf2[4 * i + 2]
        data8[i] = buf3[3 * i + 2]
        data9[i] = buf1[4 * i + 3]
        data10[i] = buf2[4 * i + 3]
    }
    buf1.release();
    buf2.release();
    buf3.release();
  }
}



void setup() {
  // put your setup code here, to run once:
  Serial.begin(38400);
  pinMode(triggerpin, INPUT);
  lastTriggerValue = LOW;
  if ( activeChannels == 1 ){
    adc1.begin(AN_RESOLUTION_16, sample_rate, dataLength, 20);
  }
  else if (activeChannels == 2 ){
    adc1.begin(AN_RESOLUTION_16, sample_rate, dataLength, 20);
    adc2.begin(AN_RESOLUTION_16, sample_rate, dataLength, 20);
  }
  else {
    adc1.begin(AN_RESOLUTION_16, sample_rate, dataLength, 20);
    adc2.begin(AN_RESOLUTION_16, sample_rate, dataLength, 20);
    adc3.begin(AN_RESOLUTION_16, sample_rate, dataLength, 20);
  }
}

void loop() {
  int actualTriggerValue = digitalRead(triggerpin);
  if actualTriggerValue == HIGH & lastTriggerValue == LOW:{
    }
  }




}

