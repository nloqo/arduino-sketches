#include <Arduino_AdvancedAnalog.h>

AdvancedADC adc(A0);
int length = 20000;
int triggerPin = 2;
bool triggerOn = false;
int triggerMeasure = 0;

void setup() {
    Serial.begin(921600);
    pinMode(2, INPUT);
    if (!adc.begin(AN_RESOLUTION_16, 200000000, length, 2 * length)) {
        Serial.println("Failed to start analog acquisition!");
        while (1);
    }
}

void loop() {
  triggerMeasure = digitalRead(triggerPin);
  if (triggerOn == false && triggerMeasure == 1){
    triggerOn = true;
    if (adc.available()){
      Serial.println("adc1 available.");
      SampleBuffer buf = adc.read();
      Serial.println(buf[0]);
      buf.release();
    }
    
  }
  else if (triggerOn == true && triggerMeasure == 0){
    triggerOn = false;
  }
}
