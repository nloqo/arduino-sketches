const String version = "ReadFlow 1.3"; // Version of this script

//-----------------------------------------Define variables and pins-----------

const int flowChannelC15 = 0;
const int tempChannelC15 = 2;

float rateC15 = 1.0;
float startTimeC15 = 0.0;
float endTimeC15 = 0.0;

float resistance = 20000.0;
float tempResistanceC15 = 0;
float tempValueC15 = 0;
float tempC15 = 0.0;

float rotationValueC15 = 0.0;
float lastValueC15 = 0.0;

// sensor parameters
const int averaging = 50;  // Number of times for the running average
int precision = 5; // Number of digits to print.
const int highVoltage = 500;
float timeC15 = 0.0;

#include "RunningAverage.h"

RunningAverage average0(averaging); //generate running average variable
RunningAverage average2(averaging); //generate running average variable

//---------------------------------------Start loop----------------------------

void sendSensorDataLast(){
    // Send the last sensor values.
    Serial.print(rateC15, precision);
    Serial.print("\t");
    Serial.print(tempC15, precision);
    Serial.println();
};

void sendSensorDataAverage(){
    // Send the running average sensor values.
    Serial.print(average0.getAverage(), precision);
    Serial.print("\t");
    Serial.print(average2.getAverage(), precision);
    Serial.println();
};

void sendSensorDataRaw(){
  // Send raw analouge input values.
  Serial.print(rotationValueC15);
    Serial.print("\t");
    Serial.print(tempValueC15);
    Serial.println();
};

void setup()
{
    Serial.begin(9600);
}

void loop()
{
    rotationValueC15 = analogRead(flowChannelC15);
    tempValueC15 = analogRead(tempChannelC15);
    endTimeC15 = millis();
    if(lastValueC15 < highVoltage && rotationValueC15 > highVoltage)
    {
      timeC15 = endTimeC15 - startTimeC15;
      rateC15 = 1 / timeC15 * 1000 / 8.8936;
      startTimeC15 = millis();
    };
    
    lastValueC15 = rotationValueC15;
    tempResistanceC15 = resistance * ((1024 / tempValueC15) - 1);     // Calculate the temperature dependent resistance in case the voltage is measured with a total voltage of 5 V  between a reference point and ground over the constant resistor.
    tempC15 = 1 / (log(tempResistanceC15 / 50000) / 3642 + 1 / 298.15) - 273.15;
    average0.addValue(rateC15);
    average2.addValue(tempC15);
    if (Serial.available()){
      char input[20];
      int len;
      len = Serial.readBytesUntil('\n', input, 20);
      input[len] = '\0';  // Null terminating the array to interpret it as a string
      switch (input[0]){
        case 'p':  // Ping
          Serial.println("pong");
          break;
        case 'l':  // Send last sensor data
          sendSensorDataLast();
          break;
        case 'q':  // Send read average sensor data
          sendSensorDataAverage();
          break;
        case 'f': //Set the precision
          precision = atoi(&input[1]);
          break;
        case 'd': // debug
          Serial.println("debug");
          break;
        case 'v':  // Send Version
          Serial.println(version);
          break;
        case 't':  // Send time
          Serial.println(timeC15);
          break;
      };
  };
}
