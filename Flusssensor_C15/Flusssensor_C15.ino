const String version = "1.3.0"; // Version of this script

//-----------------------------------------Define variables and pins-----------

const int flowChannelC15 = 0;
const int flowChannelRegen = 1;
const int tempChannelC15 = 2;
const int tempChannelRegen = 3;

float rateRegen = 0.0;
float rateC15 = 0.0;
unsigned long startTime = 0.0;
unsigned long timeEnd = 0.0;

float tempVoltageRegen = 0.0;
float tempVoltageC15 = 0.0;
int resistance = 20000;
float tempResistanceRegen = 0;
float tempResistanceC15 = 0;

int rotationsRegen = 0;
int rotationValueRegen = 0;
int tempValueRegen = 0;
int rotationsC15 = 0;
int rotationValueC15 = 0;
int tempValueC15 = 0;
int lastValueRegen = 0;
int lastValueC15 = 0;
int firstCycle = 1;

// sensor parameters
long measurements = 100; //How many times should the sensor be read for a single readout?
const int averaging = 50;  // Number of times for the running average
int precision = 5; // Number of digits to print.
int highVoltage = 500;

RunningAverage average0(averaging); //generate running average variable
RunningAverage average1(averaging); //generate running average variable
RunningAverage average2(averaging); //generate running average variable
RunningAverage average3(averaging); //generate running average variable

//---------------------------------------Start loop----------------------------

void setup()
{
    Serial.begin(9600);
}

void loop()
{
    startTime = micros();
    for(int x=1; x <= measurements; x++)
    {
        rotationValueRegen = analogRead(flowChannelRegen)
        tempValueRegen += analogRead(tempValueRegen)
        rotationValueC15 = analogRead(flowChannelC15)
        tempValueC15 += analogRead(tempChannelC15)
        if(firstCycle==0):
        {
            if(lastvalueRegen < highVoltage && rotationValueRegen > highVoltage)
            {
                rotationsRegen++;
            }
            if(lastvalueRegen > highVoltage && rotationValueRegen < highVoltage)
            {
                rotationsRegen++;
            }
            if(lastvalueC15 < highVoltage && rotationValueC15 > highVoltage)
            {
                rotationsC15++;
            }
            if(lastvalueC15 > highVoltage && rotationValueC15 < highVoltage)
            {
                rotationsC15++;
            }
        }
        lastValueC15 = rotationValueC15;
        lastValueRegen = rotationValueRegen;
        firstCycle = 0;
    }
    endTime = micros()
    tempVoltageRegen = tempValueRegen / measurements * 5 / 1024;
    tempVoltageC15 = tempValueC15 / measurements * 5 / 1024;
    tempResistanceRegen = resistance * (5 - tempVoltageRegen) / tempVoltageRegen;
    tempResistanceC15 = resistance * (5 - tempVoltageC15);
    rateRegen = rotationsRegen / 2 / (endTime - startTime) * 1e6;  
    rateC15 = rotationsC15 / 2 / (endTime - startTime) * 1e6;
    average0.addValue(rateC15)
    average1.addValue(rateRegen)
    average2.addValue(tempResistanceC15)
    average3.addValue(tempResistanceRegen)
    if (Serial.available()){
      char input[20];
      int len;
      len = Serial.readBytesUntil('\n', input, 20);
      input[len] = '\0';  // Null terminating the array to interpret it as a string
      switch (input[0]){
        case 'p':  // Ping
          Serial.println("pong");
          break;
        case 'r':  // Send read average sensor data
          sendSensorDataAverage();
          break;
        case 'm':  //Set number of measurements
          measurements = atoi(&input[1]);
          break;
        case 'f': //Set the precision
          precision = atoi(&input[1]);
          break;
        case 'd': // debug
          Serial.println("debug");
          break;
        case 'v':  // Send Version
          Serial.print(__FILE__);  // file name at compilation
          Serial.print(" ");
          Serial.print(version);
          Serial.print(" ");
          Serial.print(__DATE__);  // compilation date
          Serial.println();
          break;
        case 'l': // Send last value
          lastData();
      };
}


void sendSensorDataAverage(){
    // Send the running average sensor values.
    Serial.print(average0.getAverage(), precision);
    Serial.print("\t");
    Serial.print(average1.getAverage(), precision);
    Serial.print("\t");
    Serial.print(average2.getAverage(), precision);
    Serial.print("\t");
    Serial.print(average3.getAverage(), precision);
    Serial.print("\t");
    Serial.println();
}

void lastData(){
  // Send the last value.
  Serial.print(rateC15);
  Serial.print("\t");
  Serial.print(rateRegen);
  Serial.print("\t");
  Serial.print(tempResistanceC15);
  Serial.print("\t");
  Serial.print(tempResistanceRegen);
  Serial.println();
}
