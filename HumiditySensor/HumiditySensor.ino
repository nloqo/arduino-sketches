/*
 ***************************
   Read humidity sensor values and send them over serial connection.
   HYT939 humidity sensor, communication via I2C.
   Address defaults to 0x28
 ***************************
*/
const String version = "1.1.1"; // Version of this script

// *****************************OPERATIONAL PARAMETERS*****************************
#include "Wire.h"
const int delay_time = 10;  // delay time in ms
//byte rval = 0x00;

struct ArrayData {
  double array[2];
};

// sensor parameters
long measurements = 10;   //How many times should the sensor be read for a single readout? old 100
const int averaging = 5;  // Number of times for the running average old 50
int precision = 5;        // Number of digits to print.

// MISC
const long rolloverThreshold = 4294965000;  //After how many milliseconds should we suspend normal operation to await millis()-rollover? (Should be at least cyclePeriod+digitalPinSetDelay below the maximum value of an unsigned long of 4.294.967.295)

// Sensor values.
float sensor0Value = 0;  //Define sensor value variable
float sensor1Value = 0;  //Define sensor value variable
float sensor2Value = 0;  //Define sensor value variable
float sensor3Value = 0;  //Define sensor value variable
float sensor4Value = 0;  //Define sensor value variable
float sensor5Value = 0;  //Define sensor value variable
float sensor6Value = 0;  //Define sensor value variable
float sensor7Value = 0;  //Define sensor value variable

#include "RunningAverage.h"

RunningAverage average0(averaging);  //generate running average variable
RunningAverage average1(averaging);  //generate running average variable
RunningAverage average2(averaging);  //generate running average variable
RunningAverage average3(averaging);  //generate running average variable
RunningAverage average4(averaging);  //generate running average variable
RunningAverage average5(averaging);  //generate running average variable
RunningAverage average6(averaging);  //generate running average variable
RunningAverage average7(averaging);  //generate running average variable

// Calculation
const double maxi = 0x3fff;  // maximum raw value to scale raw value
const double TFACTOR = 165;
const double TDELTA = -40;
const double HFACTOR = 100;

void setup() {
  //Setup the serial connection and the pins
  Serial.begin(9600);
  //Wire.begin();
}


double *readout(int sensor) {
  //ArrayData a;
  static double a[2];
  unsigned char result[4];
  int i = 0;
  unsigned int traw;
  unsigned int hraw;
  double temp;
  double hum;
  int address;

  Wire.begin();
  switch (sensor) {
    case 0:
      sensor = 0x25;
      break;
    case 1:
      sensor = 0x26;
      break;
    case 2:
      sensor = 0x27;
      break;
    case 3:
      sensor = 0x28;
      break;
  };

  Wire.beginTransmission(sensor);

  Wire.endTransmission();
  delay(delay_time);
  Wire.requestFrom(sensor, 4, true);

  i = 0;
  while (Wire.available()) {
    char c = Wire.read();
    result[i] = c;
    i++;
  }
  traw = (result[2] << 8) + result[3];
  hraw = (result[0] << 8) + result[1];

  //Daten laut Datenblatt maskieren
  traw &= 0xfffc;
  hraw &= 0x3fff;
  traw >>= 2;

  //Rohdaten Umrechnen
  temp = traw / maxi * TFACTOR + TDELTA;
  hum = hraw / maxi * HFACTOR;


  a[0] = temp;
  a[1] = hum;
  return a;
};


void loop() {
  double temp0;
  double hum0;
  double temp1;
  double hum1;
  double temp2;
  double hum2;
  double temp3;
  double hum3;

  double *buffer0;
  double *buffer1;
  double *buffer2;
  double *buffer3;
  int i = 0;
  /*
  // Read the sensor many times and calculate the average
  sensor0Value = 0;
  sensor1Value = 0;
  sensor2Value = 0;
  sensor3Value = 0;
  sensor4Value = 0;
  sensor5Value = 0;
  sensor6Value = 0;
  sensor5Value = 0;
  for (int x = 1; x <= measurements; x++) {
    buffer0 = readout(0);
    buffer1 = readout(1);
    buffer2 = readout(2);
    buffer3 = readout(3);

    temp0 = buffer0[0];
    temp1 = buffer1[0];
    temp2 = buffer2[0];
    temp3 = buffer3[0];

    hum0 = buffer0[1];
    hum1 = buffer1[1];
    hum2 = buffer2[1];
    hum3 = buffer3[1];

    sensor0Value += temp0;
    sensor1Value += hum0;
    sensor2Value += temp1;
    sensor3Value += hum1;
    sensor4Value += temp2;
    sensor5Value += hum2;
    sensor6Value += temp3;
    sensor7Value += hum3;
  }

  sensor0Value /= measurements;
  sensor1Value /= measurements;
  sensor2Value /= measurements;
  sensor3Value /= measurements;
  sensor4Value /= measurements;
  sensor5Value /= measurements;
  average0.addValue(sensor0Value);
  average1.addValue(sensor1Value);
  average2.addValue(sensor2Value);
  average3.addValue(sensor3Value);
  average4.addValue(sensor4Value);
  average5.addValue(sensor5Value);
 // Serial.println(temp0);
*/
  //Serial communication
  if (Serial.available()) {
    char input[20];
    int len;
    len = Serial.readBytesUntil('\n', input, 20);
    input[len] = '\0';  // Null terminating the array to interpret it as a string
    switch (input[0]) {
      case 'p':  // Ping
        Serial.println("pong");
        break;
      case 'r':  // Send read average sensor data
        sendSensorDataAverage();
        break;
      case 'l':  // Send last sensor data
        sendSensorDataLast();
        break;
      case 'm':  //Set number of measurements
        measurements = atoi(&input[1]);
        break;
      case 'f':  //Set the precision
        precision = atoi(&input[1]);
        break;
      case 'd':  // debug
        {
          Serial.print("debug, sensor: ");
          int sensor = atoi(&input[1]);
          Serial.print(sensor);
          Serial.println();

          Wire.begin();
          switch (sensor) {
            case 0:
              {
                Wire.beginTransmission(0x25);
                Serial.print("0x25");
                break;
              }
            case 1:
              {
                Wire.beginTransmission(0x26);
                Serial.print("0x26");
                break;
              }
            case 2:
              {
                Wire.beginTransmission(0x27);
                Serial.print("0x27");
                break;
              }
            case 3:
              {
                Wire.beginTransmission(0x28);
                Serial.print("0x28");
                break;
              }
          };

          unsigned char result[4];
          int i = 0;
          unsigned int traw;
          unsigned int hraw;
          double temp;
          double hum;

          //Wire.begin();
          Serial.println();
          delay(delay_time);
          Wire.endTransmission();
          delay(delay_time);

          switch (sensor) {
            case 0:
              {
                Wire.requestFrom(0x25, 4, true);
                Serial.print("0x25");
                break;
              }
            case 1:
              {
                Wire.requestFrom(0x26, 4, true);
                Serial.print("0x26");
                break;
              }
            case 2:
              {
                Wire.requestFrom(0x27, 4, true);
                Serial.print("0x27");
                break;
              }
            case 3:
              {
                Wire.requestFrom(0x28, 4, true);
                Serial.print("0x28");
                break;
              }
          };


          i = 0;
          while (Wire.available()) {
            char c = Wire.read();
            result[i] = c;
            i++;
          }
          Serial.print("result: 0 1 2 3");

          Serial.println();
          Serial.print(result[0]);
          Serial.print(" ");
          Serial.print(result[1]);
          Serial.print(" ");
          Serial.print(result[2]);
          Serial.print(" ");
          Serial.print(result[3]);
          Serial.println();

          traw = result[2] * 256 + result[3];
          hraw = result[0] * 256 + result[1];
          Serial.print("nicht maskiert: traw: ");
          Serial.print(traw);
          Serial.print(", hraw: ");
          Serial.print(hraw);
          Serial.println();

          //Daten laut Datenblatt maskieren
          traw &= 0xfffc;
          hraw &= 0x3fff;
          traw >>= 2;
          Serial.print("maskiert: traw: ");
          Serial.print(traw);
          Serial.print(", hraw: ");
          Serial.print(hraw);
          Serial.println();

          //Rohdaten Umrechnen
          temp = traw / maxi * TFACTOR + TDELTA;
          Serial.println(temp);
          hum = hraw / maxi * HFACTOR;

          Serial.print("temp: ");
          Serial.print(temp);
          Serial.print(", hum: ");
          Serial.print(hum);
          Serial.println();
          break;
        }
      case 'x':  // another debug
        {
          int sensor = atoi(&input[1]);
          double *readout_result;
          readout_result = readout(sensor);
          double temp = readout_result[0];
          double hum = readout_result[1];
          Serial.print("sensor: ");
          Serial.print(sensor);
          Serial.print(", temp: ");
          Serial.print(temp);
          Serial.print(", hum: ");
          Serial.print(hum);
          Serial.println();
          break;
        }
      case 'y':  // demo readout
        {
          int sensor = atoi(&input[1]);
          double *readout_result;
          readout_result = readout(sensor);
          double temp = readout_result[0];
          double hum = readout_result[1];
          Serial.print(temp, precision);
          Serial.print("\t");
          Serial.print(hum, precision);
          Serial.println();
          break;
        }
      case 'v':  // Send Version
        Serial.print(__FILE__);  // file name at compilation
        Serial.print(" ");
        Serial.print(version);
        Serial.print(" ");
        Serial.print(__DATE__);  // compilation date
        Serial.println();
        break;
    };
  }

  //if we are within 2300ms of rollover of millis counter suspend normal operation
  if (millis() > rolloverThreshold) {
    while (millis() > rolloverThreshold) {};  //wait for rollover to happen
  };
}

void sendSensorDataLast() {
  // Send the last sensor values.
  Serial.print(sensor0Value, precision);
  Serial.print("\t");
  Serial.print(sensor1Value, precision);
  Serial.print("\t");
  Serial.print(sensor2Value, precision);
  Serial.print("\t");
  Serial.print(sensor3Value, precision);
  Serial.print("\t");
  Serial.print(sensor4Value, precision);
  Serial.print("\t");
  Serial.print(sensor5Value, precision);
  Serial.print("\t");
  Serial.print(sensor6Value, precision);
  Serial.print("\t");
  Serial.print(sensor7Value, precision);
  Serial.println();
}

void sendSensorDataAverage() {
  // Send the running average sensor values.
  Serial.print(average0.getAverage(), precision);
  Serial.print("\t");
  Serial.print(average1.getAverage(), precision);
  Serial.print("\t");
  Serial.print(average2.getAverage(), precision);
  Serial.print("\t");
  Serial.print(average3.getAverage(), precision);
  Serial.print("\t");
  Serial.print(average4.getAverage(), precision);
  Serial.print("\t");
  Serial.print(average5.getAverage(), precision);
  Serial.print("\t");
  Serial.print(average6.getAverage(), precision);
  Serial.print("\t");
  Serial.print(average7.getAverage(), precision);
  Serial.println();
}
