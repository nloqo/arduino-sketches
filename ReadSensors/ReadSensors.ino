/*
 ***************************
 * Read sensor values and send them over serial connection.
 ***************************
*/
const String version = "1.1.1"; // Version of this script. Filename is added before, date of compilation afterwards.


// *****************************OPERATIONAL PARAMETERS*****************************

// I/O-pins
const int sensor0Pin = 0; //Channel to which the sensor 0 is connected
const int sensor1Pin = 1; //Channel to which the sensor 1 is connected
const int sensor2Pin = 2; //Channel to which the sensor 2 is connected
const int sensor3Pin = 3; //Channel to which the sensor 2 is connected
const int sensor4Pin = 4; //Channel to which the sensor 2 is connected
const int sensor5Pin = 5; //Channel to which the sensor 2 is connected
const int powerPin = 13; //Pin for indicating device is on; also used to indicate errors

// sensor parameters
long measurements = 100; //How many times should the sensor be read for a single readout?
const int averaging = 50;  // Number of times for the running average
int precision = 5; // Number of digits to print.

// MISC
const long rolloverThreshold = 4294965000;  //After how many milliseconds should we suspend normal operation to await millis()-rollover? (Should be at least cyclePeriod+digitalPinSetDelay below the maximum value of an unsigned long of 4.294.967.295)
// ********************************************************************************


// ***********************************VARIABLES************************************
// Sensor values.
float sensor0Value = 0; //Define sensor value variable
float sensor1Value = 0; //Define sensor value variable
float sensor2Value = 0; //Define sensor value variable
float sensor3Value = 0; //Define sensor value variable
float sensor4Value = 0; //Define sensor value variable
float sensor5Value = 0; //Define sensor value variable


#include "RunningAverage.h"

RunningAverage average0(averaging); //generate running average variable
RunningAverage average1(averaging); //generate running average variable
RunningAverage average2(averaging); //generate running average variable
RunningAverage average3(averaging); //generate running average variable
RunningAverage average4(averaging); //generate running average variable
RunningAverage average5(averaging); //generate running average variable

// ********************************************************************************


void setup() {
  //Setup the serial connection and the pins
  Serial.begin(9600);

  pinMode(powerPin, OUTPUT);

  digitalWrite(powerPin, HIGH);
}


void loop() {
  // Read the sensor many times and calculate the average
  sensor0Value = 0;
  sensor1Value = 0;
  sensor2Value = 0;
  sensor3Value = 0;
  sensor4Value = 0;
  sensor5Value = 0;
  for (int x = 1; x <= measurements; x++) {
    sensor0Value += analogRead(sensor0Pin);
    sensor1Value += analogRead(sensor1Pin);
    sensor2Value += analogRead(sensor2Pin);
    sensor3Value += analogRead(sensor3Pin);
    sensor4Value += analogRead(sensor4Pin);
    sensor5Value += analogRead(sensor5Pin);
  }
  sensor0Value /= measurements;
  sensor1Value /= measurements;
  sensor2Value /= measurements;
  sensor3Value /= measurements;
  sensor4Value /= measurements;
  sensor5Value /= measurements;
  average0.addValue(sensor0Value);
  average1.addValue(sensor1Value);
  average2.addValue(sensor2Value);
  average3.addValue(sensor3Value);
  average4.addValue(sensor4Value);
  average5.addValue(sensor5Value);
  
  //Serial communication
  if ( Serial.available() ){
    char input[20];
    int len;
    len = Serial.readBytesUntil('\n', input, 20);
    input[len] = '\0';  // Null terminating the array to interpret it as a string
    switch (input[0]) {
      case 'p':  // Ping
        Serial.println("pong");
        break;
      case 'r':  // Send read average sensor data
        sendSensorDataAverage();
        break;
      case 'l':  // Send last sensor data
        sendSensorDataLast();
        break;
      case 'm':  //Set number of measurements
        measurements = atoi(&input[1]);
        break;
      case 'f':  //Set the precision
        precision = atoi(&input[1]);
        break;
      case 'd': // debug
        Serial.println("debug");
        break;
      case 'v':  // Send Version
        Serial.print(__FILE__);  // file name at compilation
        Serial.print(" ");
        Serial.print(version);
        Serial.print(" ");
        Serial.print(__DATE__);  // compilation date
        Serial.println();
        break;
    };
  }

  //if we are within 2300ms of rollover of millis counter suspend normal operation
  if (millis() > rolloverThreshold) {
    while (millis() > rolloverThreshold) {};  //wait for rollover to happen
  };
}


void sendSensorDataLast(){
    // Send the last sensor values.
    Serial.print(sensor0Value, precision);
    Serial.print("\t");
    Serial.print(sensor1Value, precision);
    Serial.print("\t");
    Serial.print(sensor2Value, precision);
    Serial.print("\t");
    Serial.print(sensor3Value, precision);
    Serial.print("\t");
    Serial.print(sensor4Value, precision);
    Serial.print("\t");
    Serial.print(sensor5Value, precision);
    Serial.println();
}


void sendSensorDataAverage(){
    // Send the running average sensor values.
    Serial.print(average0.getAverage(), precision);
    Serial.print("\t");
    Serial.print(average1.getAverage(), precision);
    Serial.print("\t");
    Serial.print(average2.getAverage(), precision);
    Serial.print("\t");
    Serial.print(average3.getAverage(), precision);
    Serial.print("\t");
    Serial.print(average4.getAverage(), precision);
    Serial.print("\t");
    Serial.print(average5.getAverage(), precision);
    Serial.println();
}
