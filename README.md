# Arduino Sketches

[![Common Changelog](https://common-changelog.org/badge.svg)](https://common-changelog.org)

Sketches for Arduinos & Co.

You can use this repository as the arduino sketchbook in the IDE.
For that reason, the sketches (`ino` files) have the be in a folder of the same name.

## Good practices

* Add a `CHANGELOG.md` file for each arduino sketch next to the main file (i.e. in the sketch folder).
* Whenever you do a change, change the version string in that file according to [semantic versioning](https://semver.org/): Major.Minor.Patch.
* If a sketch allows serial communication, you should be able to read the currently installed sketch.
  Several programs use `v\n` to request the version.
  The version should contain the Filename at compilation, the version, and the compilation date (for cross referencing a not mentioned change).
  For that, you can use the following lines:
  ```
  // Top of the file
  const String version = "1.3.0"; // Version of this script. Filename is added before, date of compilation afterwards.

  // wherever serial communication is handled
  Serial.print(__FILE__);  // file name at compilation time
  Serial.print(" ");
  Serial.print(version);  // the version string
  Serial.print(" ");
  Serial.print(__DATE__);  // compilation date
  Serial.println();
  ```

## Alternative: Telemetrix

[Telemetrix}(https://mryslab.github.io/telemetrix/) can be an alternative to writing another Arduino sketch:
You load the telemetrix sketch onto the Arduino and then you can use the python package to directly read/write the Arduino pins etc.
For some applications that might be more useful than a sketch reading pin values.


# List of sketches with their functions

In alphabetischer Reihenfolge sind die verschiedenen Sketches und ihre Funktionen erklärt


## Feuchtigkeitssensoren

Enthält Informationen zur Ansteuerung der Feuchtigkeitssensoren HYT-939


## Flusssensor

Vom ps-Projekt. Genaue Tätigkeit unbekannt. Scheit Durchflusssensoren der Kühlwasserschläuche zu beobachten und auf einem Display auszugeben (aber einiges auskommentiert).


## HumiditySensor

Arduino Due Sketch zur Detektion von bis zu 4 HYT-939 Temperatur- und Luftfeuchtigkeitssensoren mit serieller Kommunikation.


## LegacyTemperatureController

Ein Sketch, wie es vorgefunden worden war, für die Steuerung der Klimaanlage (Ventil auf und zu) abhängig von der gemessenen Raumtemperatur an einem Ort mittels einem Thermistor (thermischen Widerstand).


## OPOTemperature

A combination of *TemperatureController* and *HumiditySensor* to control the temperature of the OPO block and read some temperatures and humidity.


## ReadSensors

Der Arduino liest verschiedene Sensoren (Widerstände) aus und gibt die Spannungen auf Anfrage raus. Dieses Skript wird für die Temperatur Sensoren der Klima-Anlage verwendet.


## ServoShutter

Bewegt einen Servo auf verschiedene Stellungen gemäß serieller Kommunikation oder externem Schalter.


## Shutterschalter

Schaltet die Shutter des Quanta-Rays.


## TemperatureController

Kontrolliert die Temperatur (oder andere Größe) mittels Schaltung von binären Steuerelementen (Ventil auf/zu) und kann einerseits Messwerte (wie *ReadSensors*) ausgeben, andererseits können Parameter (Solltemperatur & Co.) eingestellt werden.

Es ist eine deutlich verbesserte Variante des *LegacyTemperatureController*.


# Tipps

## Allgemeine Resourcen

- <https://www.arduino.cc/> Webseite, dort gibt es auch die IDE
- <https://www.arduino.cc/reference/en/>  Language reference
- <https://wokwi.com/>  Ein Arduino Simulator (inklusive Breadboard, seriellem Monitor...)


## Sonderheiten von verschiedenen Boards

- Arduino Due
  - Läuft mit 3,3 V, nicht 5 V!
  - Der "Programming port" hat einen Seriellen Wandler und kommuniziert seriell mit dem Hauptchip. Er kann auch den Speicher des Hauptchips über Hardware Verbindungen löschen, falls der Hauptchip sich aufgehängt hat.
  - Der "native port" ist eine echte USB Verbindung (siehe [unten](#native-usb-ports)). Ein Reset hier funktioniert hier über die Software.


## Serielle Kommunikation

Die USB-Seriell Wandler lassen sich intern mit "Serial" ansprechen, siehe [Dokumentation](https://www.arduino.cc/reference/en/language/functions/communication/serial/).

### Native USB Ports

* Vorhanden z.B. bei Arduino Due/Arduino Zero
* Sie müssen im Code mit "SerialUSB" angesprochen werden, dann ist es direkte serielle USB Kommunikation.
  * Baud rate ist egal (da USB).
  * Übertragung ist schneller.
  * VISA Verbindung wie gehabt (z.B: "COM14").
* Können auch zur Programmierung verwendet werden.

