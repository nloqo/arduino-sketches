/*
****************************
ServoShutter

Open or close a shutter with a servo.
You can enable an external trigger (m1) to an external switch. m? queries the manual mode.
Or you can send a command to set the shutter (o0, o1), which disables the external switch (also m0 without shutter change).
o? queries the current output state (independent whether it has been set via command or by the external switch).
A LED indicates whether the output is open.

"l" returns the current state and manual mode (tab separated)
****************************
*/
const String version = "1.2.0"; // Version of this script

// Servo setup
#include <Servo.h> 
Servo servo;
const int servoPin1 = 9;
const int switchPin = 2;
const int LED_PIN = LED_BUILTIN;

// Variables
bool manual = true;  // manual mode
int state = -1;  // state of the shutter, negative is unknown
int val;  // Pin value


void setup()
{
  Serial.begin(9600);
  servo.attach(servoPin1,600,2250); //Parallax: 750 bis 2250 µs, aber keine gescheite Nullstellung mit 750µs...
  pinMode(LED_PIN, OUTPUT);
  digitalWrite(LED_PIN, LOW);
  pinMode(switchPin, INPUT_PULLUP);

  // Read initial state
  delay(100);
  if ( !digitalRead(switchPin) ){
    state = -2;
  }
}


void loop()
{
  if ( Serial.available() ) {
    char input[20];
    int len;
    len = Serial.readBytesUntil('\n', input, 20);
    input[len] = '\0';  // Null terminating the array to interpret it as a string
    switch (input[0]) {
      case 'p':  // Ping
        Serial.println("pong");
        break;
      case 'v':  // Send Version
        Serial.print(__FILE__);  // file name at compilation
        Serial.print(" ");
        Serial.print(version);
        Serial.print(" ");
        Serial.print(__DATE__);  // compilation date
        Serial.println();
        break;
      case 'l': // return last state
        Serial.print(state);
        Serial.print("\t");
        Serial.print(manual);
        Serial.println();
        break;
      case 'o': // Set output
        if ( input[1] == '0' ){  // disable seed
          closeShutter();
          manual = false;
        }
        else if ( input[1] == '1' ){  // enable seed
          openShutter();
          manual = false;
        }
        else if ( input[1] == 'm' ){  // enable external switch
          // deprecated!
          manual = true;
        }
        else if ( input[1] == '?' ){
          Serial.print(state);
          Serial.println();
        }
        break;
      case 'm': // set manual mode
        if ( input[1] == '0' ){  // disable manual mode
          manual = false;
        }
        else if ( input[1] == '1' ){  // enable manual mode
          manual = true;
        }
        else if ( input[1] == '?' ){
          Serial.print(manual);
          Serial.println();
        }
        break;
    };
  }

  if ( manual ){
    val = digitalRead(switchPin);
    // If the analog value changed from the known state or known switch, change shutter.
    if( !val && (state == 0 || state == -1))
    {
      openShutter();
    }
    else if( val && (state == 1 || state == -2))
    {
      closeShutter();
    }
  }
  delay(10);
}


void openShutter()
{
  digitalWrite(LED_PIN, HIGH);
  servo.write(110);
  state = 1;
}


void closeShutter()
{
  digitalWrite(LED_PIN, LOW);
  servo.write(80);
  state = 0;
}
