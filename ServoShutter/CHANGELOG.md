# CHANGELOG

## 1.2.0

### Changed

- Switch uses internal pullup for defined behavior.

### Added

- A LED (default internal LED) indicates whether the shutter is open.


## 1.1.0

### Added

- Commands `m0`, `m1`, `m?` to disable, enable, and query manual mode of the external switch.
- Command `o?` to query the state of the output.


## 1.0.0 before 29.04.2024

_Initial version._
