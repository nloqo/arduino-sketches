#include "Wire.h" 
int del = 10;
byte rval = 0x00;

void setup()
{
  
  Serial.begin(9600);
}

void loop() 
{
  if (Serial.available() > 0) {
    #define TFACTOR 99.2909
    #define TDELTA  40.0
    #define HFACTOR 163.83
    unsigned int traw0;
    unsigned int hraw0;
    double temp0;
    double hum0;
    unsigned int traw1;
    unsigned int hraw1;
    double temp1;
    double hum1;
    unsigned int traw2;
    unsigned int hraw2;
    double temp2;
    double hum2;
    unsigned int traw3;
    unsigned int hraw3;
    double temp3;
    double hum3;
    unsigned char buffer0[4];
    unsigned char buffer1[4];
    unsigned char buffer2[4];
    unsigned char buffer3[4];
    int i;
    
    Wire.begin();
    /*     
      Wire.beginTransmission(0x25);
      Wire.endTransmission();
        delay(del);
      Wire.requestFrom(0x25, 4, true);
      i = 0;
      while (Wire.available()) 
      {
        char c = Wire.read();  
        buffer0[i] = c;
        i++;
      }
      traw0 = buffer0[2] * 256 + buffer0[3];
      hraw0 = buffer0[0] * 256 + buffer0[1];
     
        delay(del);
     */    
      Wire.beginTransmission(0x26);
      Wire.endTransmission();
        delay(del);
      Wire.requestFrom(0x26, 4, true);
      i = 0;
      while (Wire.available()) 
      {
        char c = Wire.read();  
        buffer1[i] = c;
        i++;
      }
      traw1 = buffer1[2] * 256 + buffer1[3];
      hraw1 = buffer1[0] * 256 + buffer1[1];
     
        delay(del);
        Wire1.begin();
     /*    
      Wire1.beginTransmission(0x27);
      Wire1.endTransmission();
        delay(del);
      Wire1.requestFrom(0x27, 4, true);
      i = 0;
      while (Wire1.available()) 
      {
        char c = Wire1.read();  
        buffer2[i] = c;
        i++;
      }
      traw2 = buffer2[2] * 256 + buffer2[3];
      hraw2 = buffer2[0] * 256 + buffer2[1];
     
        delay(del);
       */   
      Wire1.beginTransmission(0x28);
      Wire1.endTransmission();
        delay(del);
      Wire1.requestFrom(0x28, 4, true);
      i = 0;
      while (Wire1.available()) 
      {
        char c = Wire1.read();  
        buffer3[i] = c;
        i++;
      }
      traw3 = buffer3[2] * 256 + buffer3[3];
      hraw3 = buffer3[0] * 256 + buffer3[1];
      
    //Daten laut Datenblatt maskieren
    traw0&=0xfffc;
    hraw0&=0x3fff;
    traw0>>=2;
    
    traw1&=0xfffc;
    hraw1&=0x3fff;
    traw1>>=2;
    
    traw2&=0xfffc;
    hraw2&=0x3fff;
    traw2>>=2;
    
    traw3&=0xfffc;
    hraw3&=0x3fff;
    traw3>>=2;  
      
    //Rohdaten Umrechnen
    temp0 = (double)traw0 / TFACTOR;
    temp0 = temp0 - TDELTA;
    hum0 = (double)hraw0 / HFACTOR;
    
    temp1 = (double)traw1 / TFACTOR;
    temp1 = temp1 - TDELTA;
    hum1 = (double)hraw1 / HFACTOR;
    
    temp2 = (double)traw2 / TFACTOR;
    temp2 = temp2 - TDELTA;
    hum2 = (double)hraw2 / HFACTOR;
    
    temp3 = (double)traw3 / TFACTOR;
    temp3 = temp3 - TDELTA;
    hum3 = (double)hraw3 / HFACTOR;
  
    Serial.print("Temp0:");
    Serial.print(temp0);
    Serial.print(';');
    Serial.print("Hum0:");
    Serial.print(hum0);
    Serial.print("Temp1:");
    Serial.print(temp1);
    Serial.print(';');
    Serial.print("Hum1:");
    Serial.print(hum1);
    Serial.print("Temp2:");
    Serial.print(temp2);
    Serial.print(';');
    Serial.print("Hum2:");
    Serial.print(hum2);
    Serial.print("Temp3:");
    Serial.print(temp3);
    Serial.print(';');
    Serial.print("Hum3:");
    Serial.print(hum3);
    Serial.print(';');
    Serial.print("Time:");
    Serial.println(millis());
    Serial.read();
    
}
}
