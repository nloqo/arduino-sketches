#include <Dhcp.h>
#include <Dns.h>
#include <Ethernet.h>
#include <EthernetClient.h>
#include <EthernetServer.h>
#include <EthernetUdp.h>
#include <SPI.h>

//HYT939 Beispiel Sketch

#define HYT939_ADDR 0x28
#define LED_PIN 13

#include <Wire.h>


void setup()
{
  //I2C Interface initialisieren,

  Wire.begin(0x28);
  Serial.begin(9600);
  Serial.println("HYT939 Beispielcode");
  pinMode(LED_PIN, OUTPUT);
}

void loop()
{

  //Skalierungsfaktoren laut Datenblatt
#define TFACTOR 99.2909
#define TDELTA  40.0
#define HFACTOR 163.83


  unsigned int traw;
  unsigned int hraw;

  double temp;
  double hum;
  int i;
  unsigned char buffer[4];

  //Messung starten
  Wire.beginTransmission(HYT939_ADDR); // transmit to device #44 (0x2c)
  Wire.endTransmission();     // stop transmitting

  //100ms warten
  delay(100);


  //4 Bytes vom Sensor lesen

  Wire.requestFrom(HYT939_ADDR, 4, true);

  i = 0;

  while (Wire.available()) {

    char c = Wire.read();    // receive a byte as character

    buffer[i] = c;

    i++;

  }

  //Rohdaten aus Puffer lesen
  traw = buffer[2] * 256 + buffer[3];
  hraw = buffer[0] * 256 + buffer[1];

  //Daten laut Datenblatt maskieren

  //traw&=0xfffc;
  //hraw&=0x3fff;
  //traw>>=2;



  //Rohdaten ausgeben, zur eventuellen Fehlersuche
  Serial.print("\r\nTemp Raw:");
  Serial.println(traw);
  Serial.print("Hum Raw:");
  Serial.println(hraw);


  //Rohdaten Umrechnen
  temp = (double)traw / TFACTOR;
  temp = temp - TDELTA;


  hum = (double)hraw / HFACTOR;

  Serial.print("Temp:");
  Serial.print(temp);
  Serial.print(';');

  Serial.print("Hum:");
  Serial.println(hum);


  //LED Blinken
  digitalWrite(LED_PIN, HIGH);
  delay(500);
  digitalWrite(LED_PIN, LOW);
  delay(500);
}





