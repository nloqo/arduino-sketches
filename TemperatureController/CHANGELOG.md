# CHANGELOG

## 2.0.0

_PID calculation changed to fraction, for upgrade reduce PID factors (Kp, Ki) by factor 1000._

### Changed

* Duty cycle is fraction of period instead per mille.
* Cycle period unit is ms instead of second, allowing 1 ms duty cycle.
* Duty cycle time resolution is µs instead of ms.


## 1.4.0

### Added

* Add a secondary control Pin (to switch both outputs at the same time)
* Add usage of the errorPin for indicating the emergency switch


## 1.3.0

### Added

* Add the duty cycle (in per mille) as a global variable (clamped to 0 to 1000) readable via debug.
