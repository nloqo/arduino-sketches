# CHANGELOG

## 1.2.1 - 2023-12-07

### Changed

- Reword "Emergency" to "Locked".


## 1.2.0 - 2023-11-13

Bug fix and change in communication.

### Changed

- Communication commands changed to w for the switch (second letter indicates the switch, third is "?" for reading or the value for writing). os/of query the shutter state.


## 1.1.0 - 2023-11-02

### Added

- Add a virtual switch via serial communication ("os"/"of" followed by -1,0,1)



## 1.0.0

_First official version._
