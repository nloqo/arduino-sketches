
/*
****************************************
* Control the shutters of the Quanta-Ray
****************************************
*/
//**             PARAMETERS           **//
const String version = "1.2.1";  // Version of this script

#include <LiquidCrystal.h>              //Library for LCD
LiquidCrystal lcd(2, 3, 4, 5, 6, 7);    //Initialize LCD Lib for our application //the numbers are the Digital Pins, which are used for the LCD

// PINs
const int fundamentalSwitchPin = 6;
const int fundamentalShutterPin = A0;
const int fundamentalLedPin = A1;
const int shgSwitchPin = 7;
const int shgShutterPin = 9;
const int shgLedPin = 8;

// Constants
const int HighLimit = 850;
const int EmergencyLimit = 20;

//**              VARIABLES          **//
// State variables
int fundamentalShutter = 0;  // state of the fundamental shutter
int shgShutter = 0;  // state of the shg shutter

int fundamentalSwitch;  // value of the fundamental switch
int shgSwitch;  // value of the shg switch
int virtual_fundamental_switch = 0;  // value of the virtual fundamental switch via serial communication
int virtual_shg_switch = 0;  // value of the virtual shg switch via serial communication


void setup(){
  //Setup the serial connection and the pins
  Serial.begin(9600);

  // Setup pins
  pinMode(fundamentalShutterPin, OUTPUT);
  pinMode(fundamentalLedPin, OUTPUT);
  pinMode(shgShutterPin, OUTPUT);
  pinMode(shgLedPin, OUTPUT);

  lcd.begin(16, 2);
  //lcd.noCursor();  // hide the cursor
  lcd.print("Fund CLOSED");
  lcd.setCursor(0, 1);
  lcd.print("SHG  CLOSED");
}


void loop(){
  // Do serial communication first
  if ( Serial.available() ){
    char input[20];
    int len;
    len = Serial.readBytesUntil('\n', input, 20);
    input[len] = '\0';  // Null terminating the array to interpret it as a string
    switch (input[0]){
      case 'p':  // Ping
        Serial.println("Pong");
        break;
      case 's': // Read the shutters
        Serial.print("FUND ");
        Serial.print(fundamentalShutter);
        Serial.print("\t");
        Serial.print("SHG ");
        Serial.print(shgShutter);
        Serial.println();
        break;
      case 'l': // list valaues
       Serial.print(fundamentalSwitch);
       Serial.print("\t");
       Serial.print(shgSwitch);
       Serial.println();
       break;
      case 'o': // read output status
        if ( input[1] == 'f'){  // fundamental
          Serial.print(fundamentalShutter);
          Serial.println();
        }
        else if (input[1] == 's'){  // fundamental
          Serial.print(shgShutter);
          Serial.println();
        }
        break;
      case 'w':  // read/write virtual switch states
        if (input[1] == 'f'){
          if (input[2] == '?'){
            Serial.print(virtual_fundamental_switch);
            Serial.println();
          }
          else {
            virtual_fundamental_switch = atoi(&input[2]);
          }
        }
        if (input[1] == 's'){
          if (input[2] == '?'){
            Serial.print(virtual_shg_switch);
            Serial.println();
          }
          else {
            virtual_shg_switch = atoi(&input[2]);
          }
        };
        break;
      case 'd': // debug
          Serial.println("Debug");
        break;
      case 'v':  // Send Version
        Serial.print(__FILE__);  // file name at compilation
        Serial.print(" ");
        Serial.print(version);
        Serial.print(" ");
        Serial.print(__DATE__);  // compilation date
        Serial.println();
        break;
    }
  }

  // Read switches
  fundamentalSwitch = analogRead(fundamentalSwitchPin);
  shgSwitch = analogRead(shgSwitchPin);

  // act on switches
  fundamentalShutter = switch_shutter(fundamentalShutterPin, 0, fundamentalSwitch, virtual_fundamental_switch, fundamentalShutter);
  shgShutter = switch_shutter(shgShutterPin, 1, shgSwitch, virtual_shg_switch, shgShutter);
}


// Switch a shutter based on the switch values and show an appropriate message on the screen.
int switch_shutter(int shutter_pin, int display_line, int analog_switch, int virtual_switch, int current_value){

  // calculate target value
  int target_value;
  if (analog_switch < EmergencyLimit or virtual_switch < 0){
    target_value = -1;
  }
  else if (analog_switch > HighLimit or virtual_switch > 0){
    target_value = 1;
  }
  else {
    target_value = 0;
  }

  // act if target_value differs from current_value
  if (target_value != current_value){
    switch (target_value){
      case 1:
        digitalWrite(shutter_pin, HIGH);
        lcd.setCursor(5, display_line);
        lcd.print("OPEN  ");
        break;
      case 0:
        digitalWrite(shutter_pin, LOW);
        lcd.setCursor(5, display_line);
        lcd.print("CLOSED");
        break;
      case -1:
        digitalWrite(shutter_pin, LOW);
        lcd.setCursor(5, display_line);
        lcd.print("LOCKED");
        break;
    }
  }

  // return the target_value as the new current_value
  return target_value;
}
