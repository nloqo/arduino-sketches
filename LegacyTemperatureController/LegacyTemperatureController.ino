/* 
 * ****************************************
 * Name:         Steuerung_Waermetauscher.ino
 * Description:  This program is used to control a cooling device,
 *               by comparing the voltage on an analog input to on/off-thesholds.
 *               These thresholds are modified depending on the voltage on a 2nd input.
 *               The first sensor should monitor the temperature fo the cooling air.
 *               The second sensor should monitor the temperature to stabilize.
 * Prequesits:   -2 thermistors and measurement resistor connected to 5V power-supply
 *                parallely; measuring voltage drop on resistor
 *               -cooling device controllabe by 5V signal
 * Author:       Andreas B�chel <andreas.buechel@physik.tu-darmstadt.de>
 * Version:      V2.1
 * ****************************************
 */


// *****************************OPERATIONAL PARAMETERS*****************************
// stabilization paramters
const float UntereLangzeitSchwelle = 513.6; //At which monitor sensor value shall the cooling sensor threshold be increased. (1 step ~ 0,1K)
const float ObereLangzeitSchwelle = 514.4; //At which monitor sensor value shall the cooling sensor threshold be decreased.
const float minimaleUntereSchaltschwelle = 490; //absolute minimal cooling threshold (hottest temp ~27�C)
const float maximaleObereSchaltschwelle = 570; //absolute maximal cooling threshold (coolest temp ~20�C)
const long minimalThresholdChangeIntervall = 80000; //minimal time in ms between threshold changes
const int switchAmplitude = 1;

// cooling operation parameters
float UntereSchaltschwelle = 524; //At which sensor value shall we switch the cooling on? (~22,7�C)
float ObereSchaltschwelle = 524; //At which sensor value shall we switch the cooling off? (~22,7�C)
const int cyclePeriod = 1000; //program cycle period in ms
const int minimalSwitchingInverval = 5000; //Minimal time after switching the cooling off, before switching it on again in ms.

// I/O-pins
const int coolingSensorChannel = 0; //Channel to which the cooling sensor is connected
const int monitorSensorChannel = 1; //Channel to which the monitor sensor is connected
const int monitorFarSensorChannel = 2; //Channel to which the monitor sensor is connected
const int controlPin = 12; //Pin by which the cooling unit is controlled
const int indicatorPin = 3; //Pin sending a control signal, indicating if the cooling is on
const int tooColdPin = 2; //Pin being set high, if the temperature is too low
const int tooHotPin = 4; //Pin being set high, if the temperature is too high
//const int monitorPin = 11; //Pin, which should be high when cooling is on and a plug is in the jack
const int powerPin = 13; //Pin for indicating device is on; also used to indicate errors


// sensor read parameters
const long numberOfValuesToAverage = 4000; //How many times should the sensor be read per second?
const int ReadDelay = 0; //How many milliseconds should we wait beween reading the sensor?

// MISC
const int digitalPinSetDelay = 1000; //Time to wait after setting a digital pin to prevent distortions on the ADC reading
const long rolloverThreashold = 4294965000;  //After how many milliseconds should we suspend normal operation to await millis()-rollover? (Should be at least cyclePeriod+digitalPinSetDelay below the maximum value of an unsigned long of 4.294.967.295)
// ********************************************************************************


// ***********************************VARIABLES************************************
//as we only want to do one measurement per second, we need to know, when we last ran
unsigned long nextFullSecond = 3000; //set starting values for an initial delay of 5s to prevent racing conditions on start
unsigned long lastFullSecond = 2000;

//these are for diagnistic purposses only
unsigned long lastMicros = 0;
unsigned long microsDiff = 0;

//we need to know the last time we switched and if cooling is currently on
unsigned long lastSwitchOffSecond = 0;
unsigned long lastSwitchThresholdChangeSecond = 0;
boolean coolingON = false;

float coolingSensorValue = 0; //Define cooling sensor value variable
float monitorSensorValue = 0; //Define sensor value variable
float monitorFarSensorValue = 0; //Define sensor value variable

boolean tooHotPinState = false;
boolean tooColdPinState = false;

#include "RunningAverage.h"

RunningAverage monitorValue(60); //generate running average variable
float monitorSensorAverage;
RunningAverage monitorFarValue(60); //generate running average variable
float monitorFarSensorAverage;
// ********************************************************************************


void setup()  {
  Serial.begin(9600);
//  while (!Serial) ; // Needed for Leonardo only
  pinMode(tooColdPin,OUTPUT);
  pinMode(tooHotPin,OUTPUT);
  pinMode(indicatorPin, OUTPUT);
  pinMode(controlPin, OUTPUT);
  pinMode(powerPin, OUTPUT);
//  pinMode(monitorPin, INPUT);

  digitalWrite(powerPin, HIGH);
  digitalWrite(indicatorPin, HIGH);
  monitorValue.clear(); //clear running average variabel
}

void loop(){
  //read the cooling sensor
  coolingSensorValue = 0;
  for(int x=1; x<=numberOfValuesToAverage; x++){
    coolingSensorValue += analogRead(coolingSensorChannel);
  }

  //read the monitor sensor
  monitorSensorValue = 0;
  for(int x=1; x<=numberOfValuesToAverage; x++){
    monitorSensorValue += analogRead(monitorSensorChannel);
  }
  //read the Far monitor sensor
  monitorFarSensorValue = 0;
  for(int x=1; x<=numberOfValuesToAverage; x++){
    monitorFarSensorValue += analogRead(monitorFarSensorChannel);
  }

  //calculate the average
  coolingSensorValue /= numberOfValuesToAverage;
  monitorSensorValue /= numberOfValuesToAverage;
  monitorFarSensorValue /= numberOfValuesToAverage;
  monitorValue.addValue(monitorSensorValue);
  monitorSensorAverage = monitorValue.getAverage();
  monitorFarValue.addValue(monitorFarSensorValue);
  monitorFarSensorAverage = monitorFarValue.getAverage();
  
  // print the results to the serial monitor:
  if (Serial){
    Serial.print(coolingSensorValue, 5);
    Serial.print("\t");
    Serial.print(monitorSensorValue, 5);
    Serial.print("\t");
    Serial.print(monitorSensorAverage, 5);
    Serial.print("\t");
    Serial.print(monitorFarSensorValue, 5);
    Serial.print("\t");
    Serial.print(monitorFarSensorAverage, 5);
    Serial.print("\t");
    Serial.print(UntereSchaltschwelle, 5);
    Serial.print("\t");
  };

  //check, if we need to switch the cooling on
  if ( coolingON == false && coolingSensorValue < UntereSchaltschwelle ) {
    //do not switch on cooling within 5s after having switched it off
    if (Serial){
      Serial.print("Should switch cooling on");
      Serial.print("\t");
    };
    if ( nextFullSecond - minimalSwitchingInverval >= lastSwitchOffSecond) {
      digitalWrite(controlPin, HIGH);
      digitalWrite(indicatorPin, HIGH);
      coolingON = true;
      if(Serial){
        Serial.print("Switching cooling on");
        Serial.print("\t");
      };
      //since the ADC may be disturbed shortly after switching a digital I/O, we wait another secoond to be sure
      nextFullSecond = nextFullSecond + digitalPinSetDelay;
    };
  };

  //check, if we need to switch the cooling off
  if ( coolingON == true && coolingSensorValue > ObereSchaltschwelle ) {
    digitalWrite(controlPin, LOW);
    digitalWrite(indicatorPin, LOW);
    coolingON = false;
    if (Serial){
      Serial.print("Switching cooling off");
      Serial.print("\t");
    };
    lastSwitchOffSecond = nextFullSecond; //set switch off time
    nextFullSecond = nextFullSecond + digitalPinSetDelay; //since the ADC may be disturbed shortly after switching a digital I/O, we wait another secoond to be sure
  };

  //check, if we are in the correct temperature range
  if ( monitorSensorAverage >= UntereLangzeitSchwelle && monitorSensorAverage <= ObereLangzeitSchwelle ) {
    if ( tooHotPinState == true ) { //turn "too hot"-LED off
      digitalWrite(tooHotPin,LOW);
      tooHotPinState = false;
    };
    if ( tooColdPinState == true ) { //turn "too cold"-LED off
      digitalWrite(tooColdPin,LOW);
      tooColdPinState = false;
    };
  };

  //check, if we need to increase the thresholds (means it is too hot)
  if ( monitorSensorAverage < UntereLangzeitSchwelle) {
    if (Serial){
      Serial.print("Should increase threshold");
      Serial.print("\t");
    };
 
    if ( tooHotPinState == false ) { //turn "too hot"-LED on
      digitalWrite(tooHotPin,HIGH);
      tooHotPinState = true;
    };
    if ( tooColdPinState == true ) { //turn "too cold"-LED off
      digitalWrite(tooColdPin,LOW);
      tooColdPinState = false;
    };

    if (nextFullSecond > minimalThresholdChangeIntervall && nextFullSecond - minimalThresholdChangeIntervall >= lastSwitchThresholdChangeSecond && ObereSchaltschwelle + switchAmplitude <= maximaleObereSchaltschwelle){
      UntereSchaltschwelle += switchAmplitude;
      ObereSchaltschwelle += switchAmplitude;
      lastSwitchThresholdChangeSecond = nextFullSecond;

      if (Serial){
        Serial.print("Increasing threshold");
        Serial.print("\t");
      };
    };
  };
       

  //check, if we need to decrease the thresholds (means, it is too cold)
  if ( monitorSensorAverage > ObereLangzeitSchwelle) {
    if (Serial){
      Serial.print("Should decrease threshold");
      Serial.print("\t");
    };

    if ( tooHotPinState == true ) { //turn "too hot"-LED off
      digitalWrite(tooHotPin,LOW);
      tooHotPinState = false;
    };
    if ( tooColdPinState == false ) { //turn "too cold"-LED on
      digitalWrite(tooColdPin,HIGH);
      tooColdPinState = true;
    };

    if (nextFullSecond > minimalThresholdChangeIntervall && nextFullSecond - minimalThresholdChangeIntervall >= lastSwitchThresholdChangeSecond && UntereSchaltschwelle - switchAmplitude >= minimaleUntereSchaltschwelle){
      UntereSchaltschwelle -= switchAmplitude;
      ObereSchaltschwelle -= switchAmplitude;
      lastSwitchThresholdChangeSecond = nextFullSecond;
      
     if (Serial){
        Serial.print("Decreaseing threshold");
        Serial.print("\t");
      };
    };
  };

  lastMicros = micros();
  if(Serial){
    Serial.print(lastFullSecond);
    Serial.print("\t");
    Serial.print(nextFullSecond);
    Serial.print("\t");
    Serial.print(millis());
    Serial.print("\t");
    Serial.print(coolingON);
    Serial.print("\t");
//    Serial.print(digitalRead(monitorPin));
//    Serial.print("\t");
    Serial.print(UntereSchaltschwelle);
    Serial.print("\t");
    Serial.print(minimalThresholdChangeIntervall);
    Serial.print("\t");
    Serial.print(lastSwitchThresholdChangeSecond);
    Serial.print("\t");
  };

  //if we are within 2300ms of rollover of millis counter suspend normal operation  
  if (millis() > rolloverThreashold){
    if(Serial){
      Serial.println("\n millis rollover!");
    };
    nextFullSecond = cyclePeriod;  //set everything for continuing normal operation after rollover
    lastFullSecond = 0;
    lastSwitchThresholdChangeSecond = 0;
    lastSwitchOffSecond = 0;
    while (millis() > rolloverThreashold){};    //wait for rollover to happen
  };
  
  //wait for beginning of next second before continuing the loop
  while(millis() <= nextFullSecond){};

  //calculate the time waited for the end of the second and print it to the serial port for diagnostic purposes
  microsDiff = micros() - lastMicros;
  lastFullSecond = nextFullSecond;
  nextFullSecond = nextFullSecond + cyclePeriod;  
  if(Serial){
    Serial.println(microsDiff);
  };
}
